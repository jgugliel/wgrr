#!/bin/zsh

textifyDuration() {
   local duration=$1
   local shiff=$duration
   local secs
   secs=$((shiff % 60));  shiff=$((shiff / 60));
   local mins
   mins=$((shiff % 60));  shiff=$((shiff / 60));
   local hours
   hours=$((shiff % 24));  shiff=$((shiff / 24));
   local days=$shiff
   if [[ $days  -gt 0 ]]; then
      txt="${days}D-${hours}h:${mins}m:${secs}s"
   elif [[ $hours -gt 0 ]]; then
      txt="${hours}h:${mins}m:${secs}s"
   elif [[ $mins -gt 0 ]]; then
      txt="${mins}m:${secs}s"
   else
      txt="${secs}s"
   fi
   echo "$txt"
}

alias awk=$1
ARRAYSIZE=$2
OUT=$3
NJOBS=$4
PRT=$6
TMP=$7
COVT=$8
IDT=$9
STIME=${10}

if [[ `hostname` != "maestro-"* ]] || [[ ${SLURM_ARRAY_TASK_ID} == "" ]] ; then
	SLURM_ARRAY_TASK_ID=$5
fi

OUTFILE="$TMP"/$OUT.wgrr_part.${SLURM_ARRAY_TASK_ID}

MAXP=$((ARRAYSIZE*SLURM_ARRAY_TASK_ID))
MINP=$((MAXP-ARRAYSIZE+1))

if [[ $STIME != "" ]] ; then
	CTIME=$(date +%s)
	printf "\r\033[K%-17s  --  [%-50s]  %s/%s  %s"  "[PROGRESS]" $(C=$((SLURM_ARRAY_TASK_ID*50/NJOBS)) ; if [ $C -eq 0 ] ; then printf "=" ; else head -c $C < /dev/zero | tr "\0" "=" ; fi) $SLURM_ARRAY_TASK_ID $NJOBS $(textifyDuration $((CTIME-STIME))) 
fi

awk -v COV=${COVT} -v ID=${IDT} -v MINP=$MINP -v MAXP=$MAXP -v OBBH=${OUT}.bbh_part.${SLURM_ARRAY_TASK_ID} -f wGRR.awk $OUT.allpairs.txt $PRT $OUT.m8 | sort -k1,1V -k2,2V > $OUTFILE

exit 0
