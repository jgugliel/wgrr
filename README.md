# wGRR - weighted Gene Repertoire Relatedness

## Description
### Introduction
`wGRR` is a set of scripts to calculate the weighted Gene Repertoire Relatedness (wGRR) index among pairs of proteins sets. wGRR relies on Best Bi-directional Hits (BBH) which are couples of proteins that are reciprocal best matches in a BLAST-like analysis. Once the BBH have been defined for two proteins sets, we can write the wGRR as
```math
wGRR(A,B) = \frac{\sum_{i}{id(A_i,B_i)}}{min(P_A,P_B)}
```
where
* $`i`$ is the number of BBH between elements $`A`$ and $`B`$
* $`id(A_i,B_i)`$ is the identity score of BBH $`i`$
* $`P_A`$ and $`P_B`$ are the numbers of proteins of $`A`$ and $`B`$ respectively

`wGRR` also calculates another similar index, the [Sørensen-Dice (SD) index][1]. Given the same BBH pairs, it is defined as
```math
SD(A,B) = \frac{2\times\sum_{i}{id(A_i,B_i)}}{P_A+P_B}
```

Finally `wGRR` optionnally calculates the [Jaccard index][2]. For this the BBH pairs are ignored, but a clustering step is added to build families for all proteins of all the input sets. Then it is defined as
```math
J(A,B) =  \frac{\left | A \cup B \right |}{\left | A \cap B \right |}
```
where $`A`$ and $`B`$ represent two sets of protein families.

### Dependencies
BBH are defined by all versus all protein comparisons using [MMseqs2][3].

Clustering (for the Jaccard index calculation) is performed by the linclust algorithm of MMseqs2.

The scripts are written in `zsh` and `awk` and as such wGRR should run on any Unix system. A recent version of `awk` is necessary, `gawk` is recommended.

wGRR can also be run on Institut Pasteur's cluster "Maestro". In this case, all dependencies will be automatically loaded upon execution.

## Installation
Download the files and make them executable (if necessary):
```bash
git clone git@gitlab.pasteur.fr:jgugliel/wGRR.git wGRR
cd wGRR
chmod +x wGRR*.zsh
```

## Usage
### Example
```bash
./wGRR -i example/test_0.prt -o TEST0
```

### On a local machine
```bash
./wGRR -i $fasta [-C $coverage_threshold -I $identity_threshold -p $mmseqs2_path -o $output_prefix -t $threads -a $comparisons -l $id_file -T -f -j -s]
```
**WARNING**  
Memory consumption can be really high and running `wGRR` might exhaust your system. By default, `wGRR` will estimate the required memory and stop if your system does not fulfill this requirement.  
It is also possible to perform a test run with the `-T` flag.

### On an interactive session on Maestro
wGRR can be used interactively on Maestro. First you need to allocate resources. For example
```bash
salloc -p hubbioit -c 10 --mem 20G
```
This will allocate 10 CPUs and 20G of memory of the hubbioit partition.

When the resource is available, wGRR can be executed as described above. Note that you do not need to load any module - `wGRR` will automatically handle this.
The number of threads passed via the `-t` option will be used for both the MMseqs step and the wGRR calculation. 

### Using `sbatch`
This is the recommended way of using wGRR for large datasets. Simply use the `sbatch` command with the proper partition specification. Note that there is no need to allocate more than one CPU (with the `sbatch` option `-c`). For example:
```bash
sbatch -p hubbioit ./wGRR -i test_2.prt -t 30
```
This will run `wGRR` on the file test_2.prt on the hubbioit partition. If you do not provide a partition, `wGRR` will use Maestro's default, *i.e.* the "common" partition with the "normal" Quality of Service (QoS).  
The MMseqs job will be submitted to the cluster's scheduler with 30 CPUs (`-t 30`). Then for the actual wGRR calculation, the required amount of jobs (depending on the value passed with the `-a` option) will be submitted to the queue. If 100 jobs (1 CPU each) are necessary, a job array of 100 jobs will be submitted to the scheduler.
To avoid using 100% of your partition's CPUs you can adjust the number of maximum jobs running simultaneously with the `-m` option.

If you do not have access to a dedicated partition, or if there is not enough free CPUs on your partition, you can try to turn on the `-f` flag. By doing so the wGRR workers will be submitted to the common and dedicated machines of Maestro, on the "fast" QoS. Jobs running on the fast QoS have a higher priority (so the workers will start faster) but are limited to 2 hours. Also, using the `-m` parameter is not necessary because in this case you will use a lot of different common resources. But you need to be sure that each worker will end in less than 2 hours otherwise the run will fail.

### Using previously generated MMSeqs2 output
It is possible to provide a MMSeqs2 file, with conditions.
1. The name of the file must be `$output_prefix.m8`, `$output_prefix` being the string passed via the `-o` option.
2. The format of the file must be: query,target,qcov,tcov,fident,evalue,bits

### Mandatory parameter
`$fasta` is a fasta file containing all the proteins of all the elements you want to compare. The protein names **must** be formatted as:
```
>Element_id_PROT
```
with `PROT` being a protein identifier, unique for each protein inside the element `Element_id`. For example:
```
>ESCO001.0321.00001.P001_00141
```
This corresponds to the protein `00141` of the element `ESCO001.0321.00001.P001`.

**Notes**:
* wGRR expects all characters after the last underscore to correspond to the protein id.
* These characters can be anything except spaces and underscores.
* The `Element_id` can have any characters (including underscores) except spaces.

### Optional parameters
* `$mmseqs2_path` should be provided if the `mmseqs` executable is not in your system path. This is not needed if you work on Maestro.

* `$output_prefix` is a prefix that will be used for each output files. By default, a random string is used.

* `$coverage_threshold` is the coverage threshold for defining the BBH pairs (float number, between 0 and 1). The default value is 0.5.

* `$identity_threshold` is the sequence identity threshold for defining the BBH pairs (float number, between 0 and 1). The default value is 0.35.

* `$threads` is the number of threads to use. Using more than one is highly recommended.

* `$comparisons` is the number of genetic elements to be compared simultaneously on each thread. A value of 1,000 means that, on each thread, groups of 1,000 genetic elements will be compared simultaneously. Increasing this number will decrease the total number of tasks to be performed but will increase RAM usage. By default, a reasonable value is automatically determined. This value should result in workers running in less than 2 hours and which is perfectly adapted to the use of the `-f` flag (see the "sbatch" section above).

* `$id_file` is a file containing one `Element_id` per line. If specified, `wGRR` will run only on these elements.

* Jaccard index caculation. By using the `-j` flag, `wGRR` will perform a protein clustering and calculate the Jaccard index.

* test run. By using the `-T` flag, a test run is executed, which omits MMseqs and wGRR calculations, and outputs some statistics of your input file to help you set up the run properly.

* If `wGRR` runs interactively (not run with `sbatch` on Maestro), the total amount of required memory will be estimated and compared to the total memory of your system. To skip this and speed up the run, you can set the `-s` flag.  
**WARNING**: if your run is not properly configured, you can completely exhaust your system.

## Output
Provided `wGRR` was executed with the `-o OUT` option, it will create several files.

1. OUT.allpairs.txt contains all pairs of genetic elements, one per line.

2. OUT.m8 is the output of `MMseqs` and contain all the significant protein-protein comparisons.

3. OUT.mmseqs.search.log is the log file of the `MMseqs` step.

4. OUT.bbh.txt contains all the identified BBH. The columns are 'query target identity query_coverage,target_coverage'

5. OUT.wgrr.log is the log file of `wGRR`

6. OUT.wgrr.txt is the final wGRR table. It contains the following columns:

The first two columns indicate the pair of elements for this row.

The third column contain the wGRR.

The fourth column contain the SD.

The fifth column contain the proportion of common proteins, _i.e._ the number of BBH pairs divided by the mean number of proteins of the two elements.

The last two columns contain the number of proteins of the two elements.

[1]: https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient
[2]: https://en.wikipedia.org/wiki/Jaccard_index
[3]: https://mmseqs.com/
