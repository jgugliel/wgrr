BEGIN {
	FS=OFS="\t"
	EVAL=1e-4
}

NR==FNR && NR>=MINP && NR<=MAXP {
	pairlist[$1" "$2]=1
	pairlist[$2" "$1]=1
	pairlist[$1" "$1]=1
	pairlist[$2" "$2]=1
	glist[$1]=1
	glist[$2]=1
}

NR==FNR && NR>=MAXP {
	nextfile
}

FILENAME==ARGV[2] && /^>/{
	split(substr($0,2),line," ")
	p=g=line[1]
	gsub(/_[^_]+$/,"",g)
	getline
	if(g in glist){
		PROTLIST[g][p]++
	}
}

FILENAME==ARGV[3] {

	if(FNR==1){
		# Delete glist because not used anymore - this allows to free some memory
		delete glist
	}

	p1=$1
	p2=$2
	g1=p1
	gsub(/_[^_]+$/,"",g1)
	g2=p2
	gsub(/_[^_]+$/,"",g2)

	if(!(g1" "g2 in pairlist)) {
		next
	}

#	My values
	cov1=$3
	cov2=$4
	fid=$5
	bits=$7

	# Fix for some evalues below awk precision
	if(evalue~/E-/){
		n=split(evalue,pwr,"-")
		if(pwr[n]>307){
			evalue=0
		}
	}

	if(cov1<=COV || cov2<=COV || fid<=ID){
		next
	}

	# Store id of all pairs
	idpair[p1][p2]=fid
	covpair[p1,p2]=cov1","cov2

	if(g1==g2 && p1==p2){
		BBS[p1][g2]=bits
		BUH[p1][g2]=p2
	}
	else if(g1!=g2){
		# prot1 is left prot - does not exist yet in the structure
		if(!(p1 in BBS)){
			BBS[p1][g2]=bits
			BUH[p1][g2]=p2
		}
		# prot1 exists but does not have a BUH in genome2 yet
		else if(!(g2 in BBS[p1])){
			BBS[p1][g2]=bits
			BUH[p1][g2]=p2
		}
		else if(p1==p2){
			BBS[p1][g2]=bits
			BUH[p1][g2]=p2
		}
		# another prot of genome1 with the same bitscore as the current BUH
		# check if id is better
		else if(bits==BBS[p1][g2] && p1!=BUH[p1][g2]){
			if(fid>idpair[p1][BUH[p1][g2]]){
				BBS[p1][g2]=bits
				BUH[p1][g2]=p2
			}
			# If 100% identical, take the smallest prot position - arbitrary, only for consistency
			else{
				pp2=BUH[p1][g2]
				n=split(pp2,array,"_")
				pos2=array[n]
				n=split(p2,array,"_")
				pos=array[n]
				if(pos<pos2){
					BBS[p1][g2]=bits
					BUH[p1][g2]=p2
				}
			}
		}
		# new prot of genome1 with greater bitscore than the current BUH
		else if(bits>BBS[p1][g2]){
			BBS[p1][g2]=bits
			BUH[p1][g2]=p2
		}
	}
}

END {
	# BBS not used anymore - free some space
	delete BBS

	# Some sequences are too short for MMseqs
	# As a result they do not even form a BBH with themselves
	# They should not be considered
	for(gi in PROTLIST){
		for(pi in PROTLIST[gi]){
			if(!(pi in BUH)){
				delete PROTLIST[gi][pi]
			}
		}
	}
	
	for(gi in PROTLIST){
		for(gj in PROTLIST){
			if(gi" "gj in pairlist){
				li=length(PROTLIST[gi])
				lj=length(PROTLIST[gj])				
				sumidR=0
				ncommonR=0
				split("",bbh,"")
				for(pi in PROTLIST[gi]){
					# pi has a BUH and pi has a BUH in gj
					if(pi in BUH && gj in BUH[pi]){
						pj=BUH[pi][gj]
						# pj has a BUH, pj has a BUH in gi, pi is the BUH of pj
						if(pj in BUH && gi in BUH[pj] && BUH[pj][gi]==pi){
							sumidR=sumidR+idpair[pi][pj]
							bbh[pi]++
							bbh[pj]++
							ncommonR++
							if(OBBH!=""){
								print pi,pj,idpair[pi][pj],covpair[pi,pj] >> OBBH
							}
						}
					}
				}

				li < lj ? mp=li : mp=lj

				if(mp==0){
					print "bad mp for genomes "gi" "gj
				}

				if(gi==gj){
					if(!MEM){
						print gi,gj,sumidR/mp,2*sumidR/(li+lj),ncommonR/li,li,lj
					}
				}
				else {
					if(gj":"gi in part_wgrr){
						avg_wgrr=(part_wgrr[gj":"gi]+sumidR/mp)/2
						avg_SD=(part_SD[gj":"gi]+(2*sumidR/(li+lj)))/2
						split(final_out[gj":"gi],ff,"\t")
						# Print twice for both directions of the wGRR
						if(!MEM){
							print ff[1],ff[2],avg_wgrr,avg_SD,ff[5],ff[6],ff[7]
							print gi,gj,avg_wgrr,avg_SD,ncommonR/((li+lj)/2),li,lj
						}
						delete part_wgrr[gj":"gi]
						delete final_out[gj":"gi]
					}
					else{
						part_wgrr[gi":"gj]=sumidR/mp
						part_SD[gi":"gj]=2*sumidR/(li+lj)
						final_out[gi":"gj]=gi"\t"gj"\t"sumidR/mp"\t"2*sumidR/(li+lj)"\t"ncommonR/((li+lj)/2)"\t"li"\t"lj
					}
				}
			}
		}
	}
	if(MEM){
		system("ps T -wo rss,command | grep wGRR.awk | grep "OUT" | sort -rn | awk '{print $1;exit}' | numfmt --from-unit=1024 --to=iec" )
	}
}
